use std::{
    borrow::Borrow,
    collections::hash_map::RandomState,
    hash::{BuildHasher, Hash},
    marker::PhantomData,
};

pub struct Cache<K, V, S = RandomState> {
    moka_cache: moka::sync::Cache<K, V, S>,
}

impl<K, V> Cache<K, V, RandomState>
where
    K: Hash + Eq + Send + Sync + 'static,
    V: Clone + Send + Sync + 'static,
{
    pub fn new(max_capacity: u64) -> Self {
        Self {
            moka_cache: moka::sync::Cache::new(max_capacity),
        }
    }
}

impl<K, V, S> Cache<K, V, S>
where
    K: Hash + Eq + Send + Sync + 'static,
    V: Clone + Send + Sync + 'static,
{
    pub fn entry(&self, key: K) -> OwnedKeyEntrySelector<K, V, S> {
        OwnedKeyEntrySelector {
            owned_key: key,
            moka_cache: &self.moka_cache,
        }
    }

    pub fn entry_by_ref<'a, Q>(&'a self, key: &'a Q) -> RefKeyEntrySelector<'a, K, Q, V, S>
    where
        K: Borrow<Q>,
        Q: ToOwned<Owned = K> + Hash + Eq + ?Sized,
    {
        RefKeyEntrySelector {
            ref_key: key,
            moka_cache: &self.moka_cache,
        }
    }
}

pub struct OwnedKeyEntrySelector<'a, K, V, S> {
    owned_key: K,
    moka_cache: &'a moka::sync::Cache<K, V, S>,
}

impl<'a, K, V, S> OwnedKeyEntrySelector<'a, K, V, S>
where
    K: Hash + Eq + Send + Sync + 'static,
    V: Clone + Send + Sync + 'static,
    S: BuildHasher + Clone + Send + Sync + 'static,
{
    pub fn or_insert_with(self, init: impl FnOnce() -> V) -> Entry<K, V> {
        let value = self.moka_cache.get_with(self.owned_key, init);
        Entry {
            key_type: PhantomData::default(), // for now
            is_fresh: true, // for now
            value,
        }
    }
}

pub struct RefKeyEntrySelector<'a, K, Q, V, S>
where
    Q: ?Sized,
{
    ref_key: &'a Q,
    moka_cache: &'a moka::sync::Cache<K, V, S>,
}

impl<'a, K, Q, V, S> RefKeyEntrySelector<'a, K, Q, V, S>
where
    K: Borrow<Q> + Hash + Eq + Send + Sync + 'static,
    Q: ToOwned<Owned = K> + Hash + Eq + ?Sized,
    V: Clone + Send + Sync + 'static,
    S: BuildHasher + Clone + Send + Sync + 'static,
{
    pub fn or_insert_with(self, init: impl FnOnce() -> V) -> Entry<K, V> {
        let owned_key: K = self.ref_key.to_owned();
        let value = self.moka_cache.get_with(owned_key, init);
        Entry {
            key_type: PhantomData::default(), // for now
            is_fresh: true, // for now
            value,
        }
    }
}

pub struct Entry<K, V> {
    // key: Arc<K>,
    key_type: PhantomData<K>,
    value: V,
    is_fresh: bool,
}

impl<K, V> Entry<K, V> {
    // pub fn key(&self) -> &K {
    //     &*self.key
    // }

    pub fn value(&self) -> &V {
        &self.value
    }

    pub fn into_value(self) -> V {
        self.value
    }

    pub fn is_fresh(&self) -> bool {
        self.is_fresh
    }
}

// pub struct FullEntry<K, V> {
//     key: Arc<K>,
//     value: V,
//     is_fresh: bool,
//     last_accessed_at: Instant,
//     last_modified_at: Instant,
//     weighted_size: u64,
// }

// impl<K, V> FullEntry<K, V> {
//     pub fn key(&self) -> &K {
//         &*self.key
//     }

//     pub fn value(&self) -> &V {
//         &self.value
//     }

//     pub fn into_value(self) -> V {
//         self.value
//     }

//     pub fn is_fresh(&self) -> bool {
//         self.is_fresh
//     }
// }
