use moka_entry_api_mock as moka_mock;

#[tokio::main]
async fn main() {
    with_sync_cache();
    with_future_cache().await;
}

fn with_sync_cache() {
    use moka_mock::sync::Cache;

    let cache: Cache<String, String> = Cache::new(100);
    let key = "key".to_string();

    let entry = cache
        .entry_by_ref(key.as_str())
        .or_insert_with(|| "value".to_string());
    dbg!(entry.is_fresh()); // bool
    // dbg!(entry.key()); //   &K
    dbg!(entry.value()); //    &V
    let _value = entry.into_value(); // V
    // dbg!(entry.value()); // Error: Borrow of moved value.

    let _value = cache
        .entry(key)
        .or_insert_with(|| "value".to_string())
        .into_value(); // V
}

async fn with_future_cache() {
    use moka_mock::future::Cache;

    let cache: Cache<String, String> = Cache::new(100);
    let key = "key".to_string();

    let entry = cache
        .entry_by_ref(key.as_str())
        .or_insert_with(async { "value".to_string() })
        .await;
    dbg!(entry.is_fresh()); // bool
    // dbg!(entry.key()); //   &K
    dbg!(entry.value()); //    &V
    let _value = entry.into_value(); // V
    // dbg!(entry.value()); // Error: Borrow of moved value.

    let _value = cache
        .entry(key)
        .or_insert_with(async { "value".to_string() })
        .await
        .into_value(); // V
}
